# encoding=utf-8
# @Author: Janho
# @Date:   2021-9-15

import pandas as pd
import numpy as np
import cv2
import random
import time

from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score

class perceptron(object):
    def __init__(self, learning_step, max_iteration):
        self.learning_step = learning_step #0.00001
        self.max_iteration = max_iteration #5000

    def train(self, features, labels):
        self.w = [0.0] * (len(features[0]) + 1)     #add b
        count_iteration = 0

        while count_iteration < self.max_iteration:
            #randomly select a feature
            index = random.randint(0, len(labels) - 1)

            x = features[index]
            x = np.append(x, 1.0)       #add b
            y = labels[index]
            wx = np.dot(self.w, x)

            # stochastic gradient descent : if it is a misclassification, change the w(with b at the last of w)
            if y * wx <= 0:
                self.w = self.w + self.learning_step * y * x      # w = w + n*y*x
                count_iteration = count_iteration + 1

        print('end')


if __name__ == '__main__':

    print('Start to read data')
    time_start = time.time()

    raw_data = pd.read_csv('../data/train_binary.csv', header=0)
    data = raw_data.values
    imgs = data[0::, 1::]
    labels = data[::, 0]

    # 选取 2/3 数据作为训练集， 1/3 数据作为测试集
    train_features, test_features, train_labels, test_labels = train_test_split(
        imgs, labels, test_size=0.33, random_state=23323)
    print(train_features.shape)

    time_read = time.time()
    print('read data cost ', time_read - time_start, ' second', '\n')

    print('Start training')
    p = perceptron(0.00001, 5000)
    p.train(train_features, train_labels)